//
//  NewsModel.swift
//  News App
//
//  Created by Rashmi HR on 10/02/21.
//

import Foundation
import ObjectMapper

class NewsModel : Mappable {
    required init?(map: Map) {
    
    }
    
    var status : String?
    var totalResults : Int?
    var articles : [Articles]?
    
    init() {
        
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        totalResults <- map["totalResults"]
        articles <- map["articles"]
    }
}

class Articles : Mappable {
    required init?(map: Map) {
    
    }
    
    var source : Source?
    var author : String?
    var title : String?
    var description : String?
    var url : String?
    var urlToImage : String?
    var publishedAt : String?
    var content : String?
    
    init() {
        
    }
    
    func mapping(map: Map) {
        
        source <- map["source"]
        author <- map["author"]
        title <- map["title"]
        description <- map["description"]
        url <- map["url"]
        urlToImage <- map["urlToImage"]
        publishedAt <- map["publishedAt"]
        content <- map["content"]
    }
    
}

class Source : Mappable {
    required init?(map: Map) {
    }
    
    var id : String?
    var name : String?
    
    init() {
        
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
    }
    
}
