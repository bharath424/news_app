//
//  ViewModel.swift
//  News App
//
//  Created by Rashmi HR on 10/02/21.
//


import Foundation
import UIKit
import RxCocoa
import RxSwift
import ObjectMapper

class ViewModel {
    
    var articles = BehaviorRelay<[Articles]>(value: [Articles()])
    var arrList: [Articles] = []
    var baseRepository: BaseRepository!
    var pageSize: Int? = 1
    var disposeBag = DisposeBag()
    
    init(baseRepository: BaseRepository) {
        self.baseRepository = baseRepository
    }
    
    func loadDoctorSlots(disposeBag: DisposeBag, completion:@escaping(Error?)->Void) {
        
        self.baseRepository.fetchAll(pageNo: String(pageSize!)).asObservable().subscribe(onNext: {[weak self] (response) in
            if let detailResponse = Mapper<NewsModel>().map(JSONObject: response) {
                print(detailResponse)
                if let article = detailResponse.articles {
                    self?.arrList.append(contentsOf: article)
                    self?.articles.accept(self!.arrList)
                }
            }
        })

    }
    
}
