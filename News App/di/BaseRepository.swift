//
//  BaseRepository.swift
//  News App
//
//  Created by Rashmi HR on 10/02/21.
//

import Foundation
import RxSwift
import RealmSwift
import ObjectMapper

class BaseRepository {
    
    let apiManager = ApiManager()
    
    func fetchAll(pageNo: String) -> Observable<Any> {
        let queryItems = [URLQueryItem(name: "country", value: "de"), URLQueryItem(name: "category", value: "business"), URLQueryItem(name: "apikey", value: "946d58e2266149c0985300ac4cd08799"), URLQueryItem(name: "page", value: pageNo), URLQueryItem(name: "pageSize", value: "10")]
        return apiManager.requestWith(queryItems: queryItems, methodName: "GET")
    }
}
