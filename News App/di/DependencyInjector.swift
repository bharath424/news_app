//
//  DependencyInjector.swift
//  News App
//
//  Created by Rashmi HR on 10/02/21.
//

import Foundation
import Swinject
import CoreLocation

class DependencyInjector {
    
    private var container = Container()
    
    public static var defaultInjector: DependencyInjector = {
        let injector = DependencyInjector()
        return injector
    }()
    
    func getContainer() -> Container {
        return container
    }
    
    func reset() {
        container = Container()
        register()
    }
    
    public func register() {
        // MARK: - Repository
//        container.register(PreferenceManager.self) { r  in
//            PreferenceManager()
//        }.inObjectScope(ObjectScope.container)
        
        container.register(ApiManager.self) { r  in
            ApiManager()
        }.inObjectScope(ObjectScope.container)
        
        container.register(BaseRepository.self) { r  in
            BaseRepository()
        }.inObjectScope(ObjectScope.container)
        
        container.register(ViewModel.self) {r in
            ViewModel(baseRepository: r.resolve(BaseRepository.self)!)
        }.inObjectScope(ObjectScope.container)
    }
}
