//
//  apiManager.swift
//  News App
//
//  Created by Rashmi HR on 10/02/21.
//

import Foundation
import RxSwift
import Alamofire
import ObjectMapper

class ApiManager {
    
    public static func sharedManager()->ApiManager{
        return DependencyInjector.defaultInjector.getContainer().resolve(ApiManager.self)!
    }
    
    static let baseUrl = URL.init(string: "https://newsapi.org/v2/top-headlines")!
    
    func requestWith( queryItems:[URLQueryItem]?=nil, methodName: String) -> Observable<Any>{
        var urlComponent = URLComponents(url:ApiManager.baseUrl, resolvingAgainstBaseURL: false)
        if let queryItems = queryItems {
            urlComponent?.queryItems = queryItems
        }
        var request = URLRequest(url: (urlComponent?.url!)!)
        request.httpMethod = methodName        
        let postReq = AF.request(request)
        return get(request: postReq)
    }
    
    func get(request:DataRequest)-> Observable<Any>{
        
        return Observable<Any>.create({
            (observer) -> Disposable in
            
            request.responseJSON(completionHandler: { (response: AFDataResponse<Any>)  in
                print("response.error\(String(describing: response.error))")
                if let error = response.error{
                    observer.onError(error)
                }else{
                    print("response.result\(response.result)")
                    switch response.result {
                    case .success(let value):
                        if let code = response.response?.statusCode, code == 200 {
                            if value is [String: Any] {
                                observer.onNext(value)
                            }
                        }
                    case .failure(_):
                        observer.onError(NSError(domain:"", code:400, userInfo:[ NSLocalizedDescriptionKey: "Something Went Wrong"]))
                        break
                    }
                    observer.onCompleted()
                }
            })
            return Disposables.create(with: {
                request.cancel()
            })
        })
    }
    
}
