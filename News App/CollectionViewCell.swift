//
//  CollectionViewCell.swift
//  News App
//
//  Created by Rashmi HR on 10/02/21.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var detailImg: UIImageView!
    @IBOutlet weak var titelLabel: UILabel!
    @IBOutlet weak var detailtext: UITextView!
    @IBOutlet weak var dateText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(article: Articles) {
        self.titelLabel.text = article.title
        self.dateText.text = article.publishedAt
        self.detailtext.text = article.description
        if let url = article.urlToImage{
            if let imageUrl = URL(string: url){
                if let data = try? Data(contentsOf: imageUrl), let image = UIImage(data: data, scale: 0) {
                    if let imageData = image.jpegData(compressionQuality: 0){
                        self.detailImg.image = UIImage(data: imageData)
                    }
                }
            }
        }
    } 

}
