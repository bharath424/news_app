//
//  ViewController.swift
//  News App
//
//  Created by Rashmi HR on 10/02/21.
//

import UIKit
import RxSwift

class ViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var segments: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    private var viewModel: ViewModel!
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        self.inject()
        super.viewDidLoad()
        tableView.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "NewsTableViewCell")
        collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CollectionViewCell")
        // Do any additional setup after loading the view.
        viewModel.loadDoctorSlots(disposeBag: disposeBag) { [weak self] (error) in
            self?.tableView.reloadData()

        }
        
        viewModel.articles.asObservable().bind(to: tableView.rx.items(cellIdentifier: "NewsTableViewCell")) { index, modal, cell in
                   let cell = cell as! NewsTableViewCell
            cell.configure(article: modal)
               }.disposed(by: self.disposeBag)
        
        self.collectionView.rx.setDelegate(self).disposed(by: disposeBag)
        
        viewModel.articles.asObservable().bind(to: collectionView.rx.items(cellIdentifier: "CollectionViewCell")) { index, model, cell in
            let cell = cell as! CollectionViewCell
            cell.configure(article: model)
        }.disposed(by: self.disposeBag)

    }

    @IBAction func viewMore(_ sender: Any) {
        if let size = viewModel.pageSize {
            viewModel.pageSize = size+1
            viewModel.loadDoctorSlots(disposeBag: disposeBag) { [weak self] (error) in
                self?.tableView.reloadData()

            }
        }
    }
    
    @IBAction func segmentSelected(_ sender: Any) {
        switch segments.selectedSegmentIndex {
        case 0:
            collectionView.isHidden = true
            tableView.isHidden = false
            break
        case 1:
            tableView.isHidden = true
            collectionView.isHidden = false
            break
        default:
            break
        }
    }
    
    func inject() {
        viewModel = DependencyInjector.defaultInjector.getContainer().resolve(ViewModel.self)
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width/0.8, height: collectionView.bounds.height/5)
    }

}

