//
//  TableViewCell.swift
//  News App
//
//  Created by Rashmi HR on 10/02/21.
//

import UIKit

class NewsTableViewCell: UITableViewCell {


    @IBOutlet weak var newsImg: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsNews: UITextView!
    @IBOutlet weak var datelabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(article: Articles) {
        self.titleLabel.text = article.title
        self.datelabel.text = article.publishedAt
        self.detailsNews.text = article.description
        if let url = article.urlToImage{
            if let imageUrl = URL(string: url){
                if let data = try? Data(contentsOf: imageUrl), let image = UIImage(data: data, scale: 0) {
                    if let imageData = image.jpegData(compressionQuality: 0){
                        self.newsImg.image = UIImage(data: imageData)
                    }
                }
            }
        }
    }    
}
